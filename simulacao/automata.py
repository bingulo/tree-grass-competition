from statistics import mean
from colorama import Back, Fore, Style
import random
import sympy as sm

# 'V' vazia
# 'A' arvore
# 'G' grama
# 'M' muda

# Grade 100x100
# Gera uma grade aleatória

class automata:


    def __init__(self, n, stress, t, numIteracoes, preserveProbs = False,
                 condInicial = {'V': 25, 'A': 25, 'G': 25, 'M': 25}):
        self.instante = 0
        self.probScale = 100

        self.stress = stress
        self.condInicial = condInicial
        self.t = t
        self.n = n
        self.numIteracoes = numIteracoes
        self.tempo = range(numIteracoes+1)
        self.funcSpreadArvore = sm.Function('gamma_arvore')(stress)
        self.funcSpreadGrama = sm.Function('gamma_grama')(stress)
        self.funcMorteArvore = sm.Function('eta_arvore')(stress)
        self.funcMorteGrama = sm.Function('eta_grama')(stress)
        self.funcMorteMuda = sm.Function('eta_muda')(stress)
    
        self.funcSpreadArvore = -2*(stress**3) + 0.8
        self.funcSpreadGrama = -3*(stress**3) + 0.4
        self.funcMorteArvore = 2**(6*(stress-1))
        self.funcMorteGrama = 2**(5*(stress-1))
        self.funcMorteMuda = 2**(5.2*(stress-1))

        if not preserveProbs:
            self.probSpreadArvore = []
            self.probSpreadGrama = []
            self.probMorteArvore = []
            self.probMorteGrama = []
            self.probMorteMuda = []
            self.probsCalc()

        self.grid = self.randomGrid(n, condInicial)
        self.genMudaAgeGrid()
        self.qtd = {'A': [], 'M': [], 'V': [], 'G': []}
        self.countCells()

    # executar isso e guardar tudo na memória, reutilizar pra implementação do numExec
    def probsCalc(self):
        print('Calculando probabilidades...')
        probScale = self.probScale
        t = self.t
        for instante in self.tempo:
            self.probSpreadArvore.append(int((probScale) * self.funcSpreadArvore.evalf(subs={t: instante})))
            self.probSpreadGrama.append(int((probScale) * self.funcSpreadGrama.evalf(subs={t: instante})))
            self.probMorteArvore.append(int((probScale) * self.funcMorteArvore.evalf(subs={t: instante})))
            self.probMorteGrama.append(int((probScale) * self.funcMorteGrama.evalf(subs={t: instante})))
            self.probMorteMuda.append(int((probScale) * self.funcMorteMuda.evalf(subs={t: instante})))
        print('Probabilidades calculadas')
        
    def genMudaAgeGrid(self):
        self.mudaAgeGrid = []
        for i in range(self.n):
            self.mudaAgeGrid.append([])
            for j in range(self.n):
                if self.grid[i][j] == 'M':
                    self.mudaAgeGrid[i].append(0)
                else:
                    self.mudaAgeGrid[i].append(-1)

    def updateMudaAgeGrid(self):
        for i in range(self.n):
            for j in range(self.n):

                if self.grid[i][j] == 'M':
                    if self.mudaAgeGrid[i][j] == 5:
                        self.mudaAgeGrid[i][j] = -1
                        self.grid[i][j] = 'A'
                    else:
                        self.mudaAgeGrid[i][j] += 1

                else:
                    self.mudaAgeGrid[i][j] = -1
                

    # probs = {V': probV...} probV = float duas casas decimais
    @staticmethod
    def randomGrid(n, condInicial):
        grid = []
        for i in range(n):
            grid.append([])
            for j in range(n):
                grid[i].append(random.sample(['V','A','G','M'], k=1,
                                             counts=[condInicial['V'],
                                                     condInicial['A'],
                                                     condInicial['G'],
                                                     condInicial['M']])[0])
        return grid
    
    def gridToInt(self):
        intGrid = []
        for i in range(self.n):
            intGrid.append([])
            for j in range(self.n):
                if self.grid[i][j] == 'V':
                    intGrid[i].append(0)
                elif self.grid[i][j] == 'A':
                    intGrid[i].append(1)
                elif self.grid[i][j] == 'M':
                    intGrid[i].append(2)
                elif self.grid[i][j] == 'G':
                    intGrid[i].append(3)                    
        return intGrid

    def randomNeighbor(self, i, j):
        random_i = i + random.choice([-2, -1, 1, 2])
        random_j = j + random.choice([-2, -1, 1, 2])

        if random_i >= self.n:
            random_i = random_i % self.n

        if random_j >= self.n:
            random_j = random_j % self.n

        return self.grid[random_i][random_j]
        
    def evalCell(self, i, j):            
        inst = self.instante
        cell = self.grid[i][j]
        if cell == 'V':
            neighborCell = self.randomNeighbor(i, j)

            if neighborCell == 'A':
                s = random.sample([0, 1], k=1, counts=[self.probScale - self.probSpreadArvore[inst],
                                                       self.probSpreadArvore[inst]])[0]
                if s == 1:
                    return 'M'
                else:
                    p = random.sample([0, 1], k=1, counts=[self.probScale - self.probSpreadGrama[inst],
                                                           self.probSpreadGrama[inst]])[0]
                    if p == 1:
                        return 'G'
                    else:
                        return 'V'

            else:
                s = random.sample([0, 1], k=1, counts=[self.probScale - self.probSpreadGrama[inst],
                                                       self.probSpreadGrama[inst]])[0]
                if s == 1:
                    return 'G'
                else:
                    return 'V'

        elif cell == 'G':
            s = random.sample([0, 1], k=1, counts=[self.probScale - self.probMorteGrama[inst],
                                                   self.probMorteGrama[inst]])[0]
            if s == 1:
                return 'V'
            else:
                return 'G'
                
        elif cell == 'M':
            s = random.sample([0, 1], k=1, counts=[self.probScale - self.probMorteMuda[inst],
                                                   self.probMorteMuda[inst]])[0]
            if s == 1:
                return 'V'
            else:
                return 'M'

        elif cell == 'A':
            s = random.sample([0, 1], k=1, counts=[self.probScale - self.probMorteArvore[inst],
                                                   self.probMorteArvore[inst]])[0]
            if s == 1:
                return 'V'
            else:
                return 'A'

    def iterate(self):
        posGrid = []
        for i in range(self.n):
            posGrid.append([])
            for j in range(self.n):
                posGrid[i].append(self.evalCell(i, j))
        self.grid = posGrid
        self.instante += 1
        self.updateMudaAgeGrid()
        self.countCells()

    def nExec(self, numExec):
        qtdN = []
        for a in range(numExec):
            self.__init__(self.n, self.stress, self.t, self.numIteracoes,
                          preserveProbs = True, condInicial = self.condInicial)
            print(str(a+1) + 'a', 'execução...')
            for i in range(self.numIteracoes):
                self.iterate()
            qtdN.append(self.qtd)
            print('Finalizada')


        if numExec != 1:
            self.qtdMedia = {'A': [], 'M': [], 'V': [], 'G': []}
            print('Calculando as médias...')
            for k in self.qtd:
                for t in self.tempo:
                    temp = []
                    for a in range(numExec):
                        temp.append(qtdN[a][k][t])
                    self.qtdMedia[k].append(mean(temp))
            print('Médias calculadas')
        else:
            self.qtdMedia = qtdN[0]

    def countCells(self):
        for k in self.qtd:
            self.qtd[k].append(0)

        for i in range(self.n):
            for j in range(self.n):
                self.qtd[self.grid[i][j]][self.instante] += 1

        n2 = (self.n ** 2) / 100
        for k in self.qtd:
            self.qtd[k][self.instante] /= n2

    


    def __str__(self):
        printableGrid = ''
        for i in range(self.n):
            for j in range(self.n):
                if self.grid[i][j] == 'A':
                    printableGrid += Fore.GREEN + Back.GREEN + 'A '
                elif self.grid[i][j] == 'M':
                    printableGrid += Fore.BLUE + Back.BLUE + 'M '
                elif self.grid[i][j] == 'G':
                    printableGrid += Fore.YELLOW + Back.YELLOW + 'G '
                elif self.grid[i][j] == 'V':
                    printableGrid += Fore.WHITE + Back.WHITE + 'V '
            printableGrid += Style.RESET_ALL + '\n'

        printableGrid += Fore.GREEN + 'Árvores: ' + str(self.qtd['A'][self.instante]) + '%\n'
        printableGrid += Fore.BLUE + 'Mudas: ' + str(self.qtd['M'][self.instante]) + '%\n'
        printableGrid += Fore.YELLOW + 'Gramas: ' + str(self.qtd['G'][self.instante]) + '%\n'
        printableGrid += Fore.WHITE + 'Vazio: ' + str(self.qtd['V'][self.instante]) + '%\n'

        return printableGrid
